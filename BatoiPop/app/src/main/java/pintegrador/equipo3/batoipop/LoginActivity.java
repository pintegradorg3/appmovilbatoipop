package pintegrador.equipo3.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bumptech.glide.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pintegrador.equipo3.batoipop.data.PreferencesManager;
import pintegrador.equipo3.batoipop.data.Utility;
import pintegrador.equipo3.batoipop.data.Usuarios;
import pintegrador.equipo3.batoipop.modelos.Usuario;

public class LoginActivity extends AppCompatActivity {

    private static AppCompatActivity appCompatActivity;

    private boolean showPassword = false;
    private EditText etEmail, etPassword;
    private Button btLogin;

    //Preferences
    /*private SharedPreferences myPreferences;
    private SharedPreferences.Editor editor;

    private static final String PREFS = "MyPrefs";
    private static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String EMAIL_KEY = "email";
    private static final String ID_KEY = "id";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appCompatActivity = this;
        setContentView(R.layout.login_page);

        setUI();
    }

    public static AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }

    private void setUI() {
        etEmail = findViewById(R.id.etLoginEmail);
        etPassword = findViewById(R.id.pwLogin);
        btLogin = findViewById(R.id.btLogIn);
        btLogin.setOnClickListener(v -> checkUserLogin());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            MainActivity.getAppCompatActivity().finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void checkUserLogin() {

        if (Utility.isNetworkAvaible()) {

            String email = etEmail.getText().toString();
            String password = etPassword.getText().toString();
            if (!email.isEmpty() || !password.isEmpty()) {
                URL url;
                try {
                    String s = "http://137.74.226.43:8080/usuarios/c=" + email;
                    url = new URL(s);
                    startBackgroundTask(url, email, password);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            } else {
               Utility.error("Hay campos vacios");
            }
        } else {
            Utility.error("Error de conexion");
        }

    }

    private void startBackgroundTask(URL url, String email, String password) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;
            final Usuario user = new Usuario();
            boolean error = false;
            boolean errorConexion = false;

            try {

                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);

                urlConnection.connect();

                getDataUser(urlConnection, user);

                if (!user.getPassword().equals(password) || !user.getCorreo().equals(email)) {
                    error = true;
                }

            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
                errorConexion = true;
            } catch (JSONException e){
                Log.i("JSONException", e.getMessage());
                error = true;
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
            boolean finalError = error;
            boolean finalErrorConexion = errorConexion;
            handler.post(() -> {
                if (finalError) {
                    Utility.error("Email o contraseña erroneos");
                } else if (finalErrorConexion) {
                    Utility.error("Conexion al servidor inaccesible");
                } else {
                    Usuarios.setUsuarioLoged(user);
                    new PreferencesManager().guardarLogin(user.getNick(), user.getPassword(), user.getCorreo(), user.getId());
                    startActivity(new Intent("pintegrador.equipo3.batoipop.STARTINGPOINT"));
                    //guardarLogin(email, password, email);
                    finish();
                }
            });
        });
    }

    /*private void guardarLogin(String username, String password, String email, int id) {
        myPreferences = getMyPreferences();

        if (myPreferences != null) {
            editor = myPreferences.edit();
            editor.putString(USERNAME_KEY, username);
            editor.putString(PASSWORD_KEY, password);
            editor.putString(EMAIL_KEY, email);
            editor.putInt(ID_KEY, id);
            editor.apply();
        }
    }

    private SharedPreferences getMyPreferences() {
        try {

            String key = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            return EncryptedSharedPreferences.create(PREFS,
                    key, MyApp.getAppContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/

    private void getDataUser(HttpURLConnection urlConnection, Usuario user) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());

            JSONObject json = new JSONObject(resultStream);
            user.setId(json.getInt("id"));
            user.setNombre(json.getString("name"));
            user.setPassword(json.getString("contrasenyaUsuario"));
            user.setTelefono(json.getString("telefonoUsuario"));
            user.setNick(json.getString("nombreUsuario"));
            user.setApellidos(json.getString("apellidosUsuario"));
            user.setCorreo(json.getString("correoUsuario"));
            user.setCodigoPostal(json.getString("codigoPostal"));
            user.setDireccion(json.getString("direccionUsuario"));
            user.setPoblacion(json.getString("poblacion"));

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }
    }


    public void showPassword(View view) {
        if (!showPassword) {
            showPassword = true;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            showPassword = false;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
    }

    public void register(View view) {
        Intent i = new Intent(this, SignInActivity.class);
        startActivity(i);
    }
}