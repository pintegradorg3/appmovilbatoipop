package pintegrador.equipo3.batoipop.modelos;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Mensaje implements Serializable {

    private String nombre;
    private Usuario usuario;
    private String mensaje;
    private String time;
    private LocalDateTime locaDate;

    public Mensaje(String mensaje, Usuario usuario){
        this.nombre = nombre;
        this.mensaje = mensaje;
        this.usuario = usuario;

        this.locaDate = LocalDateTime.now();
        this.time = locaDate.getHour() + ":" + locaDate.getMinute();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getLocaDate() {
        return locaDate;
    }

    public void setLocaDate(LocalDateTime locaDate) {
        this.locaDate = locaDate;
    }

    public Mensaje(){
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
