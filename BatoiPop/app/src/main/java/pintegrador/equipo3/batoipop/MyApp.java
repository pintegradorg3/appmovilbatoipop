package pintegrador.equipo3.batoipop;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class MyApp extends Application {

    private static Application sApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
//        SystemClock.sleep(1000);
    }

    public static Application getsApplication() {
        return sApplication;
    }

    public static Context getAppContext() {
        return getsApplication().getApplicationContext();
    }
}
