package pintegrador.equipo3.batoipop.pages.mensajes;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.adapter.AdapterChats;
import pintegrador.equipo3.batoipop.data.Chats;
import pintegrador.equipo3.batoipop.modelos.Chat;
import pintegrador.equipo3.batoipop.pages.mensajes.Chat.ChatActivity;

public class MensajesFragment extends Fragment implements AdapterChats.OnClickListener {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        TODO Crear adapter recycler view chats
        return inflater.inflate(R.layout.fragment_mensajes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView chatsRecycler = view.findViewById(R.id.recyclerChats);
        chatsRecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false));
        chatsRecycler.setAdapter(new AdapterChats(Chats.getChats(), this));
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View.OnClickListener onClick(Chat chat) {
        return v -> {
            Intent i = new Intent(MainActivity.getAppCompatActivity().getApplicationContext(), ChatActivity.class);
            i.putExtra("chat", chat.getId());
            startActivity(i);
        };
    }
}