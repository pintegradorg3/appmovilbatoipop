package pintegrador.equipo3.batoipop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.modelos.Categoria;

public class AdapterCategorias extends RecyclerView.Adapter<AdapterCategorias.HomeCategoriasViewHolder> {

    public interface OnClickListener {
        View.OnClickListener onClick(Categoria categoria);
    }

    private final List<Categoria> categorias;
    private final OnClickListener clickListener;
    private int layout;

    public AdapterCategorias(List<Categoria> categorias, OnClickListener clickListener, int layout) {
        this.categorias = categorias;
        this.clickListener = clickListener;
        this.layout = layout;
    }

    @NonNull
    @Override
    public HomeCategoriasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new HomeCategoriasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeCategoriasViewHolder holder, int position) {
        holder.bind(categorias.get(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return categorias.size();
    }

    static class HomeCategoriasViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final ImageView icon;

        public HomeCategoriasViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.categoria_nombre);
            icon = itemView.findViewById(R.id.categoria_icono);
        }

        public void bind(Categoria categoria, OnClickListener clickListener) {
            name.setText(categoria.getName());
            //icon.setImageBitmap(categoria.getImagen());
            switch (categoria.getId()) {
                case 1: icon.setImageResource(R.drawable.ic_categoria_animales); break;
                case 4: icon.setImageResource(R.drawable.ic_category_sport); break;
                case 7: icon.setImageResource(R.drawable.ic_categoria_camara); break;
                case 8: icon.setImageResource(R.drawable.ic_categoria_hogar); break;
                case 9: icon.setImageResource(R.drawable.ic_categoria_vehiculo); break;
            }
            //icon.setImageResource(R.drawable.ic_notifications_black_24dp);
            this.itemView.setOnClickListener(clickListener.onClick(categoria));
        }
    }

}
