package pintegrador.equipo3.batoipop.pages.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.data.Categorias;
import pintegrador.equipo3.batoipop.adapter.AdapterCategorias;
import pintegrador.equipo3.batoipop.adapter.AdapterProductos;
import pintegrador.equipo3.batoipop.modelos.Articulo;
import pintegrador.equipo3.batoipop.modelos.Categoria;

public class HomeFragment extends Fragment implements AdapterCategorias.OnClickListener {

    private AdapterProductos ap;
    private List<Articulo> articulos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        articulos = Articulos.getArticulos();
        ap.updateData(articulos);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView homeCategoriasRecycler = view.findViewById(R.id.recyclerCategorias);
        homeCategoriasRecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), RecyclerView.HORIZONTAL, false));
        homeCategoriasRecycler.setAdapter(new AdapterCategorias(Categorias.getCategorias(), this, R.layout.home_categoria_card_view));

        RecyclerView homeArticulosRecycler = view.findViewById(R.id.recyclerProductos);
        homeArticulosRecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false));
        articulos = new ArrayList<>();
        ap = new AdapterProductos(articulos);
        homeArticulosRecycler.setAdapter(ap);

        Button btBuscar = view.findViewById(R.id.btBuscar);
        btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.getAppCompatActivity().getApplicationContext(), SearchProductActivity.class));
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View.OnClickListener onClick(Categoria categoria) {
        return v -> {
            Intent i = new Intent(MainActivity.getAppCompatActivity().getApplicationContext(), CategorySearch.class);
            i.putExtra("categoria", categoria);
            startActivity(i);
        };
    }
}