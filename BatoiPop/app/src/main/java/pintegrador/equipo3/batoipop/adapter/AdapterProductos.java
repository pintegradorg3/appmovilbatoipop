package pintegrador.equipo3.batoipop.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.MessageFormat;
import java.util.List;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.modelos.Articulo;
import pintegrador.equipo3.batoipop.pages.home.ProductDetailActivity;

public class AdapterProductos extends RecyclerView.Adapter<AdapterProductos.HomeArticulosViewHolder> {

    private final List<Articulo> articulos;

    public AdapterProductos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateData(List<Articulo> articulos) {
        this.articulos.clear();
        this.articulos.addAll(articulos);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HomeArticulosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.productos_card_view, parent, false);
        return new HomeArticulosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeArticulosViewHolder holder, int position) {
        holder.bind(articulos.get(position));
    }

    @Override
    public int getItemCount() {
        return articulos.size();
    }

    static class HomeArticulosViewHolder extends RecyclerView.ViewHolder {

        private ImageView imagen, favorito;
        private TextView nombre, precio;

        public HomeArticulosViewHolder(@NonNull View itemView) {
            super(itemView);
            imagen = itemView.findViewById(R.id.imageProducto);
            favorito = itemView.findViewById(R.id.iconoFavorito);
            nombre = itemView.findViewById(R.id.articuloNombre);
            precio = itemView.findViewById(R.id.articuloPrecio);
        }

        public void bind(Articulo articulo) {
//            Glide.with(this.itemView).load("https://random.imagecdn.app/200/125").into(imagen);
            if (articulo.getImagen() != null) {
                imagen.setImageBitmap(articulo.getImagen());
            } else {
                imagen.setImageResource(R.drawable.logo_basico);
            }
//            imagen.setImageResource(R.drawable.logo_basico);
            nombre.setText(articulo.getNombre());
            precio.setText(MessageFormat.format("{0}€", articulo.getPrecioSimple()));
            favorito.setImageResource(isFavorito(articulo));

            this.favorito.setOnClickListener(v -> {
                if (articulo.isFavorito()) {
                    articulo.setFavorito(false);
                    this.favorito.setImageResource(R.drawable.ic_favorite_no_selected_item);
                } else {
                    articulo.setFavorito(true);
                    this.favorito.setImageResource(R.drawable.ic_favorite_selected_item);
                }
            });

            this.itemView.setOnClickListener(v -> {
//                    TODO Llamar a la activity DetailProduct pasandole el id por el bundle
                Intent i = new Intent(MainActivity.getAppCompatActivity().getApplicationContext(), ProductDetailActivity.class);
                i.putExtra("id", articulo.getId());
                MainActivity.getAppCompatActivity().startActivity(i);
//                Log.i("PRODUCT_LISTENER", String.valueOf(articulo.getId()));
            });
        }

        private int isFavorito(Articulo articulo) {
            if (articulo.isFavorito()) {
                return R.drawable.ic_favorite_selected_item;
            } else {
                return R.drawable.ic_favorite_no_selected_item;
            }
        }
    }

}
