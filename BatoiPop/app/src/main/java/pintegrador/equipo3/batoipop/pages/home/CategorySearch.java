package pintegrador.equipo3.batoipop.pages.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.adapter.AdapterProductos;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.modelos.Articulo;
import pintegrador.equipo3.batoipop.modelos.Categoria;

public class CategorySearch extends AppCompatActivity {
    private Categoria categoria;
    private TextView tvCategoria;
    private RecyclerView rvArticulosSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_search);

        categoria = (Categoria) getIntent().getSerializableExtra("categoria");

        setUI();
    }

    private void setUI() {
        tvCategoria = findViewById(R.id.tvCategoria);
        rvArticulosSearch = findViewById(R.id.recyclerArticulosSearchCategoria);
        rvArticulosSearch.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        List<Articulo> articulos = Articulos.getArticulosCategoria(categoria);
        Log.i("Prueba", articulos.toString());
        rvArticulosSearch.setAdapter(new AdapterProductos(Articulos.getArticulosCategoria(categoria)));
        tvCategoria.setText(categoria.getDescripcion());
    }


    public void back(View view) {
        finish();
    }
}