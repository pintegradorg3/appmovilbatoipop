package pintegrador.equipo3.batoipop.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.res.ResourcesCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pintegrador.equipo3.batoipop.MyApp;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.modelos.Categoria;

public class Categorias {
    private static List<Categoria> categorias;

    public static List<Categoria> getCategorias() {
        if (categorias == null) {
            categoriasFromServer();
        }
        return categorias;
    }

    private static void categoriasFromServer() {
//        TODO Crear la llamada a la api para recibir las categorias

//        Ejemplo para pruebas
        categorias = new ArrayList<>();

        /*for (int i = 0; i < 15; i++) {
            categorias.add(new Categoria(i, ("Categoria " + i), ("Categoria " + i)));
        }*/
        if (Utility.isNetworkAvaible()) {
            try {
                URL url;
                String s = "http://137.74.226.43:8080/categoria/";
                url = new URL(s);
                startBackgroundTask(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Utility.error("Error de conexion");
        }
    }

    private static void startBackgroundTask(URL url) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;
            final List<Categoria> categoriasFinal = new ArrayList<>();

            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);
                urlConnection.connect();

                getDataCategorias(urlConnection, categoriasFinal);
                categorias.addAll(categoriasFinal);
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        });
    }

    private static void getDataCategorias(HttpURLConnection urlConnection, List<Categoria> categoriasFinal) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());

            JSONArray jsonArray = new JSONArray(resultStream);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    categoriasFinal.add(new Categoria(
                            item.getInt("id"),
                            item.getString("name"),
                            item.getString("descripcionCategoria")
                    ));
                    //categoriasFinal.get(i).setImagen(BitmapFactory.);
                    /*int finalI = i;
                    switch (categoriasFinal.get(i).getId()) {
                        case 1:

                            Glide.with(MyApp.getAppContext())
                                    .asBitmap().load(AppCompatResources.getDrawable(MyApp.getAppContext(), R.drawable.ic_categoria_animales)).into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    categoriasFinal.get(finalI).setImagen(resource);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });
                            break;
                        case 4:
                            Glide.with(MyApp.getAppContext())
                                    .asBitmap().load(AppCompatResources.getDrawable(MyApp.getAppContext(), R.drawable.ic_category_sport)).into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    categoriasFinal.get(finalI).setImagen(resource);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });
                            break;
                    }*/
                    /*if (categoriasFinal.get(i).getId() == 1) {
                        int finalI = i;
                        Glide.with(MyApp.getAppContext())
                                .asBitmap().load(AppCompatResources.getDrawable(MyApp.getAppContext(), R.drawable.ic_categoria_animales)).into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                categoriasFinal.get(finalI).setImagen(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {

                            }
                        });
                    }*/
                }
            }

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }
    }

    public static Categoria getCategoriaById(int id) {
        final Categoria[] aux = {null};
        categorias.forEach(x -> {
            if (x.getId() == id) aux[0] = x;
        });
        return aux[0];
    }

    public static void load() {
        categoriasFromServer();
    }
}
