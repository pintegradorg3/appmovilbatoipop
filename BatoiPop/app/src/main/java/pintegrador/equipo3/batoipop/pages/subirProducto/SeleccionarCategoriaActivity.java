package pintegrador.equipo3.batoipop.pages.subirProducto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.adapter.AdapterCategorias;
import pintegrador.equipo3.batoipop.data.Categorias;
import pintegrador.equipo3.batoipop.modelos.Categoria;

public class SeleccionarCategoriaActivity extends AppCompatActivity implements AdapterCategorias.OnClickListener {

    private RecyclerView recyclerViewCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_categoria);

        setUI();
    }

    private void setUI() {
        recyclerViewCategoria = findViewById(R.id.recyclerCategoriaSubir);
        recyclerViewCategoria.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerViewCategoria.setAdapter(new AdapterCategorias(Categorias.getCategorias(), this, R.layout.subir_producto_categoria_cardview));
    }

    public void back(View view) {
        finish();
    }

    @Override
    public View.OnClickListener onClick(Categoria categoria) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.getAppCompatActivity().getApplicationContext(), SubirProductoActivity.class);
                i.putExtra(MainActivity.CATEGORIA, categoria);
                startActivity(i);
                finish();
            }
        };
    }
}