package pintegrador.equipo3.batoipop.pages.usuario;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.MessageFormat;

import pintegrador.equipo3.batoipop.LoginActivity;
import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.MyApp;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.data.PreferencesManager;
import pintegrador.equipo3.batoipop.data.Usuarios;
import pintegrador.equipo3.batoipop.modelos.Usuario;

public class UsuarioFragment extends Fragment {

    private TextView tvNombre, tvCerrarSesion, tvUbicacion, tvTelefono;
    private Usuario user;
    private ImageView ivUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_usuario, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        user = Usuarios.getUserLoged();
        tvNombre = view.findViewById(R.id.tvUserPageNombre);
        ivUser = view.findViewById(R.id.ivUserPage);
        tvUbicacion = view.findViewById(R.id.tvUserPageUbicacionActual);
        tvTelefono = view.findViewById(R.id.tvUserPageTlf);

        tvNombre.setText(MessageFormat.format("{0} {1}", user.getNombre(), user.getApellidos()));
        ivUser.setImageBitmap(user.getImagen());
        tvUbicacion.setText(MessageFormat.format("{0}, {1}", user.getDireccion(), user.getPoblacion()));
        tvTelefono.setText(user.getTelefono());

        tvCerrarSesion = view.findViewById(R.id.tvUserCerrarSesion);
        tvCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.getAppCompatActivity().startActivity(new Intent(MainActivity.getAppCompatActivity().getApplicationContext(), LoginActivity.class));
                new PreferencesManager().clear();
                MainActivity.getAppCompatActivity().finish();
            }
        });
    }
}