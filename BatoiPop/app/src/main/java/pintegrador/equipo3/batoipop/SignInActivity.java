package pintegrador.equipo3.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pintegrador.equipo3.batoipop.data.PreferencesManager;

public class SignInActivity extends AppCompatActivity {
    private boolean showPassword = false;
    private EditText etUsername, etEmail, etPassword;
    private Button btSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_page);

        setUI();
    }

    private void setUI() {
        etUsername = findViewById(R.id.etSignInUsername);
        etEmail = findViewById(R.id.etSignInEmail);
        etPassword = findViewById(R.id.pwSignIn);
        btSignIn = findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(v -> registerUser());
    }

    private void registerUser() {
        String username = etUsername.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        if (username.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Hay campos vacios", Toast.LENGTH_LONG).show();
        } else {
            //new PreferencesManager().guardarLogin(username, password, email);
            LoginActivity.getAppCompatActivity().finish();
            finish();
        }

    }

    public void showPassword(View view) {
        if (!showPassword) {
            showPassword = true;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            showPassword = false;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }

    }

    public void back(View view) {
        finish();
    }
}