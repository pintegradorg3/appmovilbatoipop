package pintegrador.equipo3.batoipop.pages.favoritos;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.adapter.AdapterProductos;
import pintegrador.equipo3.batoipop.modelos.Articulo;


public class FavoriteFragment extends Fragment {

    private AdapterProductos ap;
    private List<Articulo> articulos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorite, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerView = view.findViewById(R.id.recyclerCategorias);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false));
        articulos = new ArrayList<>();
        ap = new AdapterProductos(articulos);
        recyclerView.setAdapter(ap);
        if (articulos.isEmpty()) {
//            Mostrar imagen de no hay favoritos
        } else {
//            Mostrar articulos
        }

        super.onViewCreated(view, savedInstanceState);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onResume() {
        super.onResume();
        articulos = Articulos.getArticulosFavoritos();
        ap.updateData(articulos);

    }
}