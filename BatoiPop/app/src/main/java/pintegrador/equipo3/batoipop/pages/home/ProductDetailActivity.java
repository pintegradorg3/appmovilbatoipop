package pintegrador.equipo3.batoipop.pages.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.MessageFormat;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.adapter.FetchAddressTask;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.modelos.Articulo;

public class ProductDetailActivity extends AppCompatActivity implements FetchAddressTask.OnTaskCompleted {

    private static final String TAG = "GEO";
    private static final int REQUEST_LOCATION_PERMISSION = 1;

    private Articulo articulo;
    private ImageView favorito, imagenArticulo, ivUser;
    private TextView nombre, descripcion, categoria, precio, tvUserName;
    private GoogleMap googleMap;
    private GoogleMapOptions googleMapOptions;
    private MapView map;
    private FusedLocationProviderClient flProvider;
    private Location lastLocation;
    private TextView tvLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Intent i = getIntent();

        articulo = Articulos.getArticuloById(i.getIntExtra("id", 0));
        favorito = findViewById(R.id.ic_favorito);

        getLocation();
        setUI();
    }

    @SuppressLint("MissingPermission")
    private void setUI() {
        imagenArticulo = findViewById(R.id.ivImagenArticulo);
        nombre = findViewById(R.id.tvNombreDetail);
        descripcion = findViewById(R.id.tvDescripcionDetail);
        categoria = findViewById(R.id.tvCategoriaDetail);
        precio = findViewById(R.id.tvPrecioDetail);
        map = findViewById(R.id.mapView);
        imagenArticulo.setImageBitmap(articulo.getImagen());
        tvUserName = findViewById(R.id.tvNameUserDetail);
        ivUser = findViewById(R.id.imageUser);





        flProvider = LocationServices.getFusedLocationProviderClient(this);
        flProvider.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    lastLocation = location;
                    new FetchAddressTask(ProductDetailActivity.this,
                            ProductDetailActivity.this).execute(location);
                    tvLocation = new TextView(ProductDetailActivity.this);
                    tvLocation.setText(getString(R.string.address_text));
                }
            }
        });

        nombre.setText(articulo.getNombre());
        descripcion.setText(articulo.getDescripcion());
        categoria.setText(articulo.getCategoria().getName());
        precio.setText(MessageFormat.format("{0} €", articulo.getPrecioSimple()));
        ivUser.setImageBitmap(articulo.getUsuarioVendedor().getImagen());
        tvUserName.setText(articulo.getUsuarioVendedor().getNick());

        if (articulo.isFavorito()) {
            favorito.setImageResource(R.drawable.ic_favorite_detail_selected);
        } else {
            favorito.setImageResource(R.drawable.ic_favorite_detail);
        }
        favorito.setOnClickListener(v -> {
            if (articulo.isFavorito()) {
                articulo.setFavorito(false);
                favorito.setImageResource(R.drawable.ic_favorite_detail);
            } else {
                articulo.setFavorito(true);
                favorito.setImageResource(R.drawable.ic_favorite_detail_selected);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {// If the permission is granted, get the location,
            // otherwise, show a Toast
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            } else {
                Toast.makeText(this, R.string.location_permission_denied,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    REQUEST_LOCATION_PERMISSION);
            Log.d(TAG, "getLocation: permissions granted");
        } else {

            Log.d(TAG, "getLocation: permissions denied");
        }
    }



    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    public void back(View view) {
        finish();
    }

    public void denuncia(View view) {

    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void onTaskCompleted(String result) {
        tvLocation.setText(getString(R.string.address_text,
                result, System.currentTimeMillis()));
    }
}