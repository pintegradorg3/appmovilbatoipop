package pintegrador.equipo3.batoipop.data;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import pintegrador.equipo3.batoipop.MyApp;
import pintegrador.equipo3.batoipop.modelos.Usuario;

public class Usuarios {

    private static List<Usuario> usuarios;
    private static Usuario usuarioLoged;

    public static List<Usuario> getUsuarios() {
        if (usuarios == null) {
            getFromServer();
        }
        return usuarios;
    }



    public static Usuario getUserById(int id) {

        Usuario aux = null;
        if (usuarios == null) usuarios = new ArrayList<>();
        if (usuarios.contains(new Usuario(id))) aux = usuarios.get(usuarios.indexOf(new Usuario(id)));
        if (aux == null) {
            getFromServer(id);
            aux = usuarios.get(0);
        }

        return aux;
    }

    private static void getFromServer() {
        if (Utility.isNetworkAvaible()) {
            try {
                final List<Usuario> usuarioFinal = new ArrayList<>();
                URL url;
                String s = "http://137.74.226.43:8080/usuarios/";
                url = new URL(s);
                startBackgroundTask(url, usuarioFinal);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Utility.error("Error de conexion");
        }
    }

    private static void getFromServer(int id) {
        if (Utility.isNetworkAvaible()) {
            try {
                final Usuario usuarioFinal = new Usuario();
                URL url;
                String s = "http://137.74.226.43:8080/usuarios/" + id;
                url = new URL(s);
                startBackgroundTask(url, usuarioFinal);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Utility.error("Error de conexion");
        }
    }

    private static void getFromServerUserLoged() {
        if (Utility.isNetworkAvaible()) {
            try {

                URL url;
                String s = "http://137.74.226.43:8080/usuarios/" + new PreferencesManager().getIdLoged();
                url = new URL(s);
                startBackgroundTask(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Utility.error("Error de conexion");
        }
    }

    private static void startBackgroundTask(URL url) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;


            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);
                urlConnection.connect();

                getDataUsuarios(urlConnection);
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        });
    }

    private static void startBackgroundTask(URL url, Usuario usuarioFinal) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;


            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);
                urlConnection.connect();

                getDataUsuarios(urlConnection, usuarioFinal);
                usuarios.add(0, usuarioFinal);
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        });
    }

    private static void startBackgroundTask(URL url, List<Usuario> usuarioFinal) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;


            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);
                urlConnection.connect();

                getDataUsuarios(urlConnection, usuarioFinal);
                usuarios.addAll(usuarioFinal);
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        });
    }

    private static void getDataUsuarios(HttpURLConnection urlConnection) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());
            if (usuarioLoged == null) usuarioLoged = new Usuario();

            JSONObject json = new JSONObject(resultStream);
            usuarioLoged.setId(json.getInt("id"));
            usuarioLoged.setNombre(json.getString("nombreUsuario"));
            usuarioLoged.setTelefono(json.getString("telefonoUsuario"));
            usuarioLoged.setNick(json.getString("name"));
            usuarioLoged.setApellidos(json.getString("apellidosUsuario"));
            usuarioLoged.setCorreo(json.getString("correoUsuario"));
            usuarioLoged.setCodigoPostal(json.getString("codigoPostal"));
            usuarioLoged.setDireccion(json.getString("direccionUsuario"));
            usuarioLoged.setPoblacion(json.getString("poblacion"));
            Glide.with(MyApp.getAppContext())
                    .asBitmap().load("https://picsum.photos/id/1062/500/500")
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            usuarioLoged.setImagen(resource);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {

                        }
                    });

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }
    }

    private static void getDataUsuarios(HttpURLConnection urlConnection, Usuario usuarioFinal) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());

            JSONObject json = new JSONObject(resultStream);
            usuarioFinal.setId(json.getInt("id"));
            usuarioFinal.setNombre(json.getString("nombreUsuario"));
            usuarioFinal.setTelefono(json.getString("telefonoUsuario"));
            usuarioFinal.setNick(json.getString("name"));
            usuarioFinal.setApellidos(json.getString("apellidosUsuario"));
            usuarioFinal.setCorreo(json.getString("correoUsuario"));
            usuarioFinal.setCodigoPostal(json.getString("codigoPostal"));
            usuarioFinal.setDireccion(json.getString("direccionUsuario"));
            usuarioFinal.setPoblacion(json.getString("poblacion"));

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }
    }

    private static void getDataUsuarios(HttpURLConnection urlConnection, List<Usuario> usuarioListFinal) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());

            JSONArray jsonArry = new JSONArray(resultStream);
            if (jsonArry.length() > 0) {
                for (int i = 0; i < jsonArry.length(); i++) {
                    JSONObject json = jsonArry.getJSONObject(i);

                    Usuario usr = new Usuario();
                    usr.setId(json.getInt("id"));
                    usr.setNombre(json.getString("nombreUsuario"));
                    usr.setTelefono(json.getString("telefonoUsuario"));
                    usr.setNick(json.getString("name"));
                    usr.setApellidos(json.getString("apellidosUsuario"));
                    usr.setCorreo(json.getString("correoUsuario"));
                    usr.setCodigoPostal(json.getString("codigoPostal"));
                    usr.setDireccion(json.getString("direccionUsuario"));
                    usr.setPoblacion(json.getString("poblacion"));

                    Glide.with(MyApp.getAppContext())
                            .asBitmap().load("https://picsum.photos/id/1062/500/500")
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    usr.setImagen(resource);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });

                    usuarioListFinal.add(usr);
                }
            }

            //JSONObject json = new JSONObject(resultStream);


            /*usr.setId(json.getInt("id"));
            usr.setNombre(json.getString("nombreUsuario"));
            usr.setTelefono(json.getString("telefonoUsuario"));
            usr.setNick(json.getString("name"));
            usr.setApellidos(json.getString("apellidosUsuario"));
            usr.setCorreo(json.getString("correoUsuario"));
            usr.setCodigoPostal(json.getString("codigoPostal"));
            usr.setDireccion(json.getString("direccionUsuario"));
            usr.setPoblacion(json.getString("poblacion"));*/

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }
    }

    public static Usuario getUserLoged() {
        //if (usuarios == null) getFromServer();
        if (usuarioLoged == null) getFromServerUserLoged();
        return usuarioLoged;
    }

    public static void setUsuarioLoged(Usuario usuario) {
        usuarioLoged = usuario;
    }

    public static void load() {
        if (usuarios == null) usuarios = new ArrayList<>();
        getFromServer();
        getFromServerUserLoged();
    }
}
