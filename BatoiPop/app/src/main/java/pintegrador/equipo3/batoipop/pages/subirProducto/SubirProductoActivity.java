package pintegrador.equipo3.batoipop.pages.subirProducto;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.data.Usuarios;
import pintegrador.equipo3.batoipop.modelos.Articulo;
import pintegrador.equipo3.batoipop.modelos.Categoria;

public class SubirProductoActivity extends AppCompatActivity {

//    Atributos producto
    private String titulo, descripcion, precio;
    private Categoria categoria;
    private int id;
    private Bitmap image;

    private ImageView imagen;
    private EditText etTitulo, etDescripcion, etPrecio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subir_producto);

        id = Articulos.getNewId();
        categoria = (Categoria) getIntent().getSerializableExtra(MainActivity.CATEGORIA);

        etPrecio = findViewById(R.id.etSubirPrecio);
        etDescripcion = findViewById(R.id.etSubirDescripcion);
        etTitulo = findViewById(R.id.etSubirTitulo);

        imagen = findViewById(R.id.ivSubirProducto);
    }

    public void addImage(View view) {
        cargarImagen();
    }

    private void cargarImagen() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/");
        startActivityForResult(i.createChooser(i, "Selecciona la Aplicacion"), 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            assert data != null;
            try {
                image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                imagen.setImageBitmap(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void close(View view) {
        finish();
    }

    public void subirProducto(View view) {
        titulo = etTitulo.getText().toString();
        descripcion = etDescripcion.getText().toString();
        precio = etPrecio.getText().toString();

        if (titulo.isEmpty() || descripcion.isEmpty() || precio.isEmpty()) {
            Toast.makeText(this, "Campos obligatorios vacios", Toast.LENGTH_LONG).show();
        } else {
            Articulo art = new Articulo(titulo, Integer.parseInt(precio), false, categoria, id);
            art.setDescripcion(descripcion);
            if (image != null) {
                art.setImagen(image);
            } else {
                art.setImagen(BitmapFactory.decodeResource(getResources(), R.drawable.logo_basico));
            }
            art.setUsuarioVendedor(Usuarios.getUserLoged());
            Articulos.subirProducto(art);
        }

        finish();

    }
}