package pintegrador.equipo3.batoipop.modelos;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.text.DecimalFormat;

public class Articulo implements Serializable {

    private int id;
    private String nombre;
    private String descripcion;
    private Usuario usuarioComprador;
    private Usuario usuarioVendedor;
    private boolean disponible;
    private Categoria categoria;
    private double precio;
    private int precioSimple;
    private Bitmap imagen;
    private boolean favorito;

    public Articulo() {
    }

    public Articulo(String nombre, double precio, boolean favorito, Categoria categoria, int id) {
        this.nombre = nombre;
        this.precio = precio;
        this.precioSimple = (int) precio;
        this.favorito = favorito;
        this.categoria = categoria;
        this.id = id;
    }

    public Articulo(int id, String nombre, String descripcion, Categoria categoria, double precio, boolean disponible) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.precio = precio;
        this.precioSimple = (int) precio;
        this.disponible = disponible;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getPrecioSimple() {
        return precioSimple;
    }

    public void setPrecioSimple(int precioSimple) {
        this.precioSimple = precioSimple;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Usuario getUsuarioComprador() {
        return usuarioComprador;
    }

    public void setUsuarioComprador(Usuario usuarioComprador) {
        this.usuarioComprador = usuarioComprador;
    }

    public Usuario getUsuarioVendedor() {
        return usuarioVendedor;
    }

    public void setUsuarioVendedor(Usuario usuarioVendedor) {
        this.usuarioVendedor = usuarioVendedor;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

//    public double getPrecio() {
//        return precio;
//    }

//    public void setPrecio(double precio) {
//        this.precio = precio;
//    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }
}
