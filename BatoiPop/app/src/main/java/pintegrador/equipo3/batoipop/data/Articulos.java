package pintegrador.equipo3.batoipop.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.MyApp;
import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.modelos.Articulo;
import pintegrador.equipo3.batoipop.modelos.Categoria;
import pintegrador.equipo3.batoipop.modelos.Usuario;

public class Articulos {
    private static List<Articulo> articulos;

    public static List<Articulo> getArticulos() {
        if (articulos == null || articulos.isEmpty()) {
            articulosFromServer();
            System.out.println("hola");
        }
        return articulos;
    }

    public static List<Articulo> getArticulosCategoria(Categoria categoria) {
        List<Articulo> aux = new ArrayList<>();
        if (articulos == null) {
            articulosFromServer();
        }

        articulos.forEach(x -> {
            if (x.getCategoria().equals(categoria)) {
                aux.add(x);
            }
        });
        return aux;
    }

    public static Articulo getArticuloById(int id) {
        final Articulo[] aux = {null};
        if (articulos == null) {
            articulosFromServer();
        }

        articulos.forEach(x -> {
            if (x.getId() == id){
                aux[0] = x;
            }
        });
        return aux[0];
    }

    public static List<Articulo> getArticulosFavoritos() {
        List<Articulo> favoritos = new ArrayList<>();
        if (articulos == null) {
            articulosFromServer();
        }

        articulos.forEach(x -> {
            if (x.isFavorito()) {
                favoritos.add(x);
            }
        });
        return favoritos;
    }

    private static void articulosFromServer() {
        //        TODO Crear la llamada a la api para recibir los articulos

        articulos = new ArrayList<>();

        /*for (int i = 0; i < 15; i++) {
            articulos.add(new Articulo("Articulo " + i, (int) (Math.random() * i), false, Categorias.getCategorias().get(0), i));
            articulos.get(i).setDescripcion("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ");
            int finalI = i;
            Glide.with(MyApp.getAppContext())
                    .asBitmap().load("https://picsum.photos/id/" + ThreadLocalRandom.current().nextInt(1, 1000) + "/300/300")
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            articulos.get(finalI).setImagen(resource);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {

                        }
                    });
            *//*if (i == 10) {

                l.add(BitmapFactory.decodeResource(MainActivity.getAppCompatActivity().getResources(), R.drawable.logo_nombre));
            } else {
                l.add(BitmapFactory.decodeResource(MainActivity.getAppCompatActivity().getResources(), R.drawable.logo_basico));
            }*//*

        }*/
        if (Utility.isNetworkAvaible()) {
            try {
                URL url;
                String s = "http://137.74.226.43:8080/article/";
                url = new URL(s);
                startBackgroundTask(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Utility.error("Error de conexion");
        }
    }

    private static void startBackgroundTask(URL url) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;
            final List<Articulo> articulosFinal = new ArrayList<>();

            try {

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);
                urlConnection.connect();

                getDataArticulos(urlConnection, articulosFinal);
                articulos.addAll(articulosFinal);
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e){
                Log.i("JSONException", e.getMessage());
                System.out.println("Fallo");
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        });
    }

    private static void getDataArticulos(HttpURLConnection urlConnection, List<Articulo> articulosFinal) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());

            JSONArray json = new JSONArray(resultStream);
            if (json.length() > 0) {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject item = json.getJSONObject(i);
                    articulosFinal.add(new Articulo(
                            item.getInt("id"),
                            item.getString("name"),
                            item.getString("descripcionArticulo"),
                            Categorias.getCategoriaById(item.getJSONObject("batoipopCategoria").getInt("id")),
                            item.getDouble("precioArticulo"),
                            item.getBoolean("disponible"))
                    );
                    int finalI = i;
                    Glide.with(MyApp.getAppContext())
                            .asBitmap().load("http://137.74.226.43:8069/web/image/?model=batoipop.articles&field=imagen&session_id=b6c881757323fbb4d81dc9f68d78fe02a85738de&id=" + articulosFinal.get(i).getId())
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    articulosFinal.get(finalI).setImagen(resource);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });
                    /*Glide.with(MyApp.getAppContext())
                            .asBitmap().load("https://picsum.photos/id/" + ThreadLocalRandom.current().nextInt(1, 1000) + "/300/300")
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    articulosFinal.get(finalI).setImagen(resource);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });*/
                }
            }

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }

    }



    public static List<Articulo> getArticulosByNombre(String n) {
        List<Articulo> aux = new ArrayList<>();
        if (articulos == null) {
            articulosFromServer();
        }

        articulos.forEach(x -> {
            if (x.getNombre().toLowerCase().contains(n.toLowerCase())) aux.add(x);
        });
        return aux;
    }

    public static int getNewId() {
        return articulos.get(articulos.size() - 1).getId() + 1;
    }

    public static void subirProducto(Articulo articulo) {
        articulos.add(0, articulo);
    }

    public static void load() {
        articulosFromServer();
    }
}
