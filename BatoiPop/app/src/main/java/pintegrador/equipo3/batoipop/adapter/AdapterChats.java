package pintegrador.equipo3.batoipop.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.modelos.Chat;

public class AdapterChats extends RecyclerView.Adapter<AdapterChats.ChatsViewHolder>{

    public interface OnClickListener {
        View.OnClickListener onClick(Chat chat);
    }

    private final List<Chat> chats;
    private OnClickListener listener;

    public AdapterChats(List<Chat> chats, OnClickListener listener) {
        this.chats = chats;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ChatsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chats_card_view, parent, false);
        return new ChatsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsViewHolder holder, int position) {
        holder.bind(chats.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    static class ChatsViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivProduct;
        private TextView tvUser, tvFecha, tvProducto, tvUltimoMensaje;

        public ChatsViewHolder(@NonNull View itemView) {
            super(itemView);
            ivProduct = itemView.findViewById(R.id.ivProduct);
            tvUser = itemView.findViewById(R.id.tvUserProductChats);
            tvFecha = itemView.findViewById(R.id.tvFechaChats);
            tvUltimoMensaje = itemView.findViewById(R.id.tvUltimoMensaje);
            tvProducto = itemView.findViewById(R.id.tvProductChats);
        }

        public void bind(Chat chat, OnClickListener listener) {
            if (chat.getArticulo().getImagen() != null) {
                ivProduct.setImageBitmap(chat.getArticulo().getImagen());
            } else {
                ivProduct.setImageResource(R.drawable.logo_basico);
            }
            //ivProduct.setImageResource(R.drawable.logo_basico);
            tvUser.setText(chat.getUsuarioRecibido().getNick());
            tvFecha.setText(chat.getFechaUltimoMensaje().toString());
            tvUltimoMensaje.setText("hola");
            tvProducto.setText(chat.getArticulo().getNombre());
            this.itemView.setOnClickListener(listener.onClick(chat));
        }
    }
}
