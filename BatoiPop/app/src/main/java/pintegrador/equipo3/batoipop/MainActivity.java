package pintegrador.equipo3.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pintegrador.equipo3.batoipop.data.PreferencesManager;
import pintegrador.equipo3.batoipop.pages.subirProducto.SeleccionarCategoriaActivity;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences myPreferences;
    private SharedPreferences.Editor editor;

    public static final String PREFS = "MyPrefs";
    public static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String EMAIL_KEY = "email";

    public static final String CATEGORIA = "categoria";
    private static AppCompatActivity appCompatActivity;
    ExecutorService executorService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appCompatActivity = this;
        /*executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            if (!userIsLoged()) {
                startActivity(new Intent(appCompatActivity.getApplicationContext(), LoginActivity.class));
            }
        });*/
        //SystemClock.sleep(1500);
        setContentView(R.layout.activity_main);
        setUI();



    }

    private boolean userIsLoged() {
        myPreferences = getMyPreferences();
        if (myPreferences != null) {
            String username = myPreferences.getString(USERNAME_KEY, "");
            System.out.println(username);
            return !username.equals("");
        }
        return false;
    }

    private SharedPreferences getMyPreferences() {

        try {
            String key = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            return EncryptedSharedPreferences.create(PREFS,
                    key, this,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
        //return getSharedPreferences(PREFS, MODE_PRIVATE);
    }


    public static AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }

    private void setUI() {

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);

        BottomNavigationItemView btSubirProducto = findViewById(R.id.navigation_subir_productos);

        btSubirProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(appCompatActivity.getApplicationContext(), SeleccionarCategoriaActivity.class));
            }
        });

    }
}