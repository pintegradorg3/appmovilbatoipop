package pintegrador.equipo3.batoipop.modelos;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Chat implements Serializable {

    private int id;
    private List<Mensaje> mensajes;
    private Articulo articulo;
    private Usuario usuarioEnviado;
    private Usuario usuarioRecibido;
    private LocalDate fechaUltimoMensaje;

    public Chat(int id, Articulo articulo, Usuario usuarioComprador, LocalDate fechaUltimoMensaje) {
        mensajes = new ArrayList<>();
        this.articulo = articulo;
        this.usuarioRecibido = usuarioComprador;
        this.usuarioEnviado = articulo.getUsuarioVendedor();
        this.fechaUltimoMensaje = fechaUltimoMensaje;
    }

    public Chat(int id, Articulo articulo, Usuario usuarioEnviado, Usuario usuarioRecibido) {
        mensajes = new ArrayList<>();
        this.id = id;
        this.articulo = articulo;
        this.usuarioEnviado = usuarioEnviado;
        this.usuarioRecibido = usuarioRecibido;
    }

    public Chat() {
        mensajes = new ArrayList<>();
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public void setUsuarioEnviado(Usuario usuarioEnviado) {
        this.usuarioEnviado = usuarioEnviado;
    }

    public void setUsuarioRecibido(Usuario usuarioRecibido) {
        this.usuarioRecibido = usuarioRecibido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void addMensaje(Mensaje mensaje) {
        mensajes.add(mensaje);
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public Usuario getUsuarioEnviado() {
        return usuarioEnviado;
    }


    public Usuario getUsuarioRecibido() {
        return usuarioRecibido;
    }


    public LocalDate getFechaUltimoMensaje() {
        return fechaUltimoMensaje;
    }

    public void setFechaUltimoMensaje(LocalDate fechaUltimoMensaje) {
        this.fechaUltimoMensaje = fechaUltimoMensaje;
    }
}
