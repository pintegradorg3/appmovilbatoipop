package pintegrador.equipo3.batoipop;

import static java.lang.Thread.sleep;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.data.Categorias;
import pintegrador.equipo3.batoipop.data.Chats;
import pintegrador.equipo3.batoipop.data.PreferencesManager;
import pintegrador.equipo3.batoipop.data.Usuarios;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startActivity(new Intent(this, MainActivity.class));
        setContentView(R.layout.activity_splash);
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                if (!new PreferencesManager().userIsLoged()) {
                    startActivity(new Intent(MyApp.getAppContext(), LoginActivity.class));
                    //startActivity(new Intent("pintegrador.equipo3.batoipop.STARTINGPOINT"));
                }
            }
        });
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Categorias.load();
                    Articulos.load();
                    Usuarios.load();
                    Chats.load();
                } finally {
                    SystemClock.sleep(2000);
                    if (new PreferencesManager().userIsLoged()) {
                        startActivity(new Intent("pintegrador.equipo3.batoipop.STARTINGPOINT"));
                    }
                }
            }
        });


    }

    /*private boolean userIsLoged() {
        SharedPreferences myPreferences = getMyPreferences();
        if (myPreferences != null) {
            String username = myPreferences.getString(MainActivity.USERNAME_KEY, "");
            return !username.equals("");
        }
        return false;
    }

    private SharedPreferences getMyPreferences() {

        try {
            String key = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            return EncryptedSharedPreferences.create(MainActivity.PREFS,
                    key, this,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
        //return getSharedPreferences(PREFS, MODE_PRIVATE);
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}