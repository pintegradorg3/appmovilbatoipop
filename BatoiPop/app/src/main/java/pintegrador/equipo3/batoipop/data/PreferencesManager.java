package pintegrador.equipo3.batoipop.data;

import android.content.SharedPreferences;
import android.util.Log;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;

import pintegrador.equipo3.batoipop.MainActivity;
import pintegrador.equipo3.batoipop.MyApp;

public class PreferencesManager {

    private final SharedPreferences myPreferences;
    private SharedPreferences.Editor editor;

    private static final String PREFS = "MyPrefs";
    private static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String EMAIL_KEY = "email";
    private static final String ID_KEY = "id";


    public PreferencesManager() {
        this.myPreferences = getMyPreferences();
    }

    public void clear() {
        if (myPreferences != null) {
            editor = myPreferences.edit();
            editor.clear();
            editor.apply();
        }
    }

    public int getIdLoged() {
        //int usrId = Usuarios.getUsuarios().get(0).getId();
        if (myPreferences != null) {
            editor = myPreferences.edit();
            return myPreferences.getInt(ID_KEY, 9);
        }
        return 9;
    }

    public void guardarLogin(String username, String password, String email, int id) {
        if (myPreferences != null) {
            editor = myPreferences.edit();
            editor.putString(USERNAME_KEY, username);
            editor.putString(PASSWORD_KEY, password);
            editor.putString(EMAIL_KEY, email);
            editor.putInt(ID_KEY, id);
            editor.apply();
        }
    }

    public boolean userIsLoged() {
        if (myPreferences != null) {
            String username = myPreferences.getString(MainActivity.USERNAME_KEY, "");
            return !username.equals("");
        }
        return false;
    }

    public SharedPreferences getMyPreferences() {
        try {
            String key = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            return EncryptedSharedPreferences.create(PREFS,
                    key, MyApp.getAppContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
