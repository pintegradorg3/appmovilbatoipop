package pintegrador.equipo3.batoipop.data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pintegrador.equipo3.batoipop.modelos.Chat;
import pintegrador.equipo3.batoipop.modelos.Mensaje;

public class Chats {

    private static List<Chat> chats;

    public static List<Chat> getChats() {
        if (chats == null) {
            chatsFromServer();
        }
        return chats;
    }

    private static void chatsFromServer() {
        chats = new ArrayList<>();

        /*for (int i = 0; i < 1; i++) {
            chats.add(new Chat(i, Articulos.getArticulos().get(i), Usuarios.getUsuarios().get(i), LocalDate.now()));
            chats.get(i).addMensaje(new Mensaje("hola", chats.get(i).getUsuarioRecibido()));
        }*/
        if (Utility.isNetworkAvaible()) {
            try {
                URL url;
                String s = "http://137.74.226.43:8080/mensaje";
                url = new URL(s);
                startBackgroundTask(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Utility.error("Error de conexion");
        }
    }

    private static void startBackgroundTask(URL url) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            HttpURLConnection urlConnection = null;
            final List<Chat> chatsFinal = new ArrayList<>();

            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(Utility.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Utility.READ_TIMEOUT);
                urlConnection.connect();

                getDataChats(urlConnection, chatsFinal);
                chats.addAll(chatsFinal);
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        });
    }

    private static void getDataChats(HttpURLConnection urlConnection, List<Chat> chatsFinal) throws IOException, JSONException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = Utility.readStream(urlConnection.getInputStream());

            JSONArray jsonArray = new JSONArray(resultStream);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    boolean usuariosNull = true;
                    JSONObject item = jsonArray.getJSONObject(i);
                    JSONObject art = item.getJSONObject("batoipopArticles");
                    JSONObject userEnviado = item.optJSONObject("batoipopUsuarioByIdUsuarioEnviado");
                    JSONObject userRecibido = item.optJSONObject("batoipopUsuarioByIdUsuarioRecibido");


                    if (Chats.exists(item.getInt("id"))) {
                        if (userRecibido == null) {
                            Chats.getById(item.getInt("id")).addMensaje(
                                    new Mensaje(item.getString("mensaje"),
                                            Usuarios.getUserLoged())
                            );
                        } else {
                            Chats.getById(item.getInt("id")).addMensaje(
                                    new Mensaje(item.getString("mensaje"),
                                            Usuarios.getUserById(userRecibido.getInt("id")))
                            );
                        }

                    } else {
                        Chat ch = new Chat();
                        ch.setId(item.getInt("id"));
                        ch.setArticulo(Articulos.getArticuloById(art.getInt("id")));
                        ch.setFechaUltimoMensaje(LocalDate.now());

                        if (userRecibido == null || userEnviado == null) {
                            ch.setUsuarioRecibido(Usuarios.getUserLoged());
                            ch.setUsuarioEnviado(Usuarios.getUsuarios().get(0));
                        } else {
                            ch.setUsuarioRecibido(Usuarios.getUserById(userRecibido.getInt("id")));
                            ch.setUsuarioEnviado(Usuarios.getUserById(userEnviado.getInt("id")));
                        }

                        ch.addMensaje(new Mensaje(item.getString("mensaje"), ch.getUsuarioEnviado()));

                        if (ch.getUsuarioEnviado().equals(Usuarios.getUserLoged())
                                || ch.getUsuarioRecibido().equals(Usuarios.getUserLoged())) chatsFinal.add(ch);
                    }


                }
            }

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }
    }

    private static boolean exists(int id) {
        final boolean[] existe = {false};
        chats.forEach(x -> {
            if (x.getId() == id) existe[0] = true;
        });
        return existe[0];
    }

    public static Chat getById(int id) {
        final Chat[] chat = new Chat[1];
        chats.forEach(x -> {
            if (x.getId() == id) {
                chat[0] = x;
            }
        });
        return chat[0];
    }

    public static void load() {
        chatsFromServer();
    }
}
