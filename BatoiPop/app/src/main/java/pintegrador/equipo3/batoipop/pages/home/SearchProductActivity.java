package pintegrador.equipo3.batoipop.pages.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.adapter.AdapterProductos;
import pintegrador.equipo3.batoipop.data.Articulos;
import pintegrador.equipo3.batoipop.modelos.Articulo;

public class SearchProductActivity extends AppCompatActivity {

    private EditText etSearch;
    private RecyclerView rvProductos;
    private List<Articulo> articulos;
    private AdapterProductos ap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        setUI();
    }

    private void setUI() {
        articulos = new ArrayList<>();
//        articulos = Articulos.getArticulosByNombre("o");
        etSearch = findViewById(R.id.etSearchProductos);
        rvProductos = findViewById(R.id.recyclerProductosSearch);
        rvProductos.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        ap = new AdapterProductos(articulos);
        rvProductos.setAdapter(ap);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchProducts();
                    return true;
                }
                return false;
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void searchProducts() {
        String busqueda = etSearch.getText().toString();
        if (busqueda.equals("") || Pattern.compile(" {2,}").matcher(busqueda).find() || busqueda.indexOf(" ") == 0) {
            error("El campo de busqueda es erroneo");
        } else {
            hideKeyboard();
            articulos = Articulos.getArticulosByNombre(busqueda);
            System.out.println(articulos);
            if (articulos.isEmpty()) error("No hay productos que coincidan con la busqueda");
            ap.updateData(articulos);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    private void error(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public void back(View view) {
        finish();
    }
}