package pintegrador.equipo3.batoipop.pages.mensajes.Chat;

import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.adapter.MensajeAdapter;
import pintegrador.equipo3.batoipop.data.Chats;
import pintegrador.equipo3.batoipop.data.Usuarios;
import pintegrador.equipo3.batoipop.modelos.Chat;
import pintegrador.equipo3.batoipop.modelos.Mensaje;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener{

    private final int PORT = 9098;
    private final String IP_SERVIDOR = "172.16.222.42";

    private RecyclerView recyclerView;
    private MensajeAdapter mensajeAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private EditText etMensaje;
    private ImageView btSend, ivProducto, ivUser;
    private TextView tvProducto, tvPrecio;

    private List<Mensaje> mensajeList;
    private Chat chat;

    private final String CLAVE = "Alex Alonso Erian Victoria", SALT = "Batoipop", ALGORITHM = "AES/CBC/PKCS5Padding", IV = "hola caracola 12";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);
//        chat = (Chat) getIntent().getSerializableExtra("chat");
        chat = Chats.getById(getIntent().getIntExtra("chat", 0));

        if(onViewCreated(savedInstanceState))
        setUI();

    }

    private void setUI() {


        btSend = findViewById(R.id.ivEnviar);

        etMensaje = findViewById(R.id.etMensaje);
        tvPrecio = findViewById(R.id.tvPrecioProductoChat);
        tvProducto = findViewById(R.id.tvNombreProducto);
        ivProducto = findViewById(R.id.ivImagenProducto);
        ivUser = findViewById(R.id.ivUserInteresado);



        tvProducto.setText(chat.getArticulo().getNombre());
        tvPrecio.setText(MessageFormat.format("{0} €", chat.getArticulo().getPrecioSimple()));
        ivProducto.setImageBitmap(chat.getArticulo().getImagen());
        if (!chat.getUsuarioRecibido().equals(Usuarios.getUserLoged())) {
            ivUser.setImageBitmap(chat.getUsuarioRecibido().getImagen());
            System.out.println("hola");
        } else {
            ivUser.setImageBitmap(chat.getUsuarioEnviado().getImagen());
            System.out.println("Adios");
        }

        btSend.setOnClickListener(this);


        mensajeList = chat.getMensajes();
        mensajeAdapter = new MensajeAdapter(mensajeList);
        recyclerView = findViewById(R.id.recyclerMensajesChat);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mensajeAdapter);

        onLine();
    }

    private void onLine() {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CHECK 1");
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            /**
             * Método creado para que el cliente esté a la escucha en _todo momento
             */
            @Override
            public void run() {
                try (ServerSocket serverSocket_cliente = new ServerSocket(PORT)){

                    Socket socketCliente;
                    SecretKey secretKey = AESCifrado.getKeyFromPassword(CLAVE, SALT);
                    IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));

                    while (true){


                        socketCliente = serverSocket_cliente.accept();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketCliente.getInputStream()));
                        String nombreUsuario = bufferedReader.readLine();
                        String mensajeCifrado = bufferedReader.readLine();
                        String mensajeDescifrado = AESCifrado.decrypt(ALGORITHM, mensajeCifrado, secretKey, iv);

                        Mensaje msn = new Mensaje(mensajeDescifrado, Usuarios.getUsuarios().get(1));

                        mensajeAdapter.add(msn);
                        layoutManager.scrollToPosition(mensajeList.size() - 1);
                    }
                } catch (IOException e) {
                    System.out.println("ERROR AL CREAR EL SOCKET DE SERVIDOR EN CLIENTE -> " + e.getMessage());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        System.out.println(chat.getMensajes());
        System.out.println(mensajeList);
        String texto = "";
        texto = this.etMensaje.getText().toString();


        String finalTexto = texto;
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                BufferedWriter bufferedWriter = null;
                IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));
                try {
                    Socket socket = new Socket(IP_SERVIDOR, PORT);
                    SecretKey secretKey = AESCifrado.getKeyFromPassword(CLAVE, SALT);
                    bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    bufferedWriter.write("1");
                    bufferedWriter.newLine();
                    bufferedWriter.write("Usser2: ");
                    bufferedWriter.newLine();
                    bufferedWriter.write(AESCifrado.encrypt(ALGORITHM, finalTexto, secretKey, iv));
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        Mensaje msn = new Mensaje(texto, Usuarios.getUserLoged());
        // chat.addMensaje(msn);
        mensajeAdapter.add(msn);
        layoutManager.scrollToPosition(mensajeList.size() - 1);

        SystemClock.sleep(100);

        Mensaje prueba = new Mensaje("hola", Usuarios.getUsuarios().get(1));
        // chat.addMensaje(prueba);
        mensajeAdapter.add(prueba);
        layoutManager.scrollToPosition(mensajeList.size() - 1);
        etMensaje.setText("");

    }

    public boolean onViewCreated(Bundle savedInstanceState){
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            return true;
        } return false;
    }

    public void backChat(View view) {
        finish();
    }
}
