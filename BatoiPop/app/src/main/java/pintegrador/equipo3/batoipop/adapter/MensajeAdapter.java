package pintegrador.equipo3.batoipop.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import pintegrador.equipo3.batoipop.R;
import pintegrador.equipo3.batoipop.data.PreferencesManager;
import pintegrador.equipo3.batoipop.data.Usuarios;
import pintegrador.equipo3.batoipop.modelos.Chat;
import pintegrador.equipo3.batoipop.modelos.Mensaje;

public class MensajeAdapter extends RecyclerView.Adapter<MensajeAdapter.MyViewHolder>{

    private List<Mensaje> listaMensajes;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View mensajeLayout;
        if (listaMensajes.get(viewType).getUsuario().equals(Usuarios.getUserLoged())) {
            mensajeLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_mensaje_enviado, parent, false);
        } else {
            mensajeLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_mensaje_recibido, parent, false);
        }
        return new MyViewHolder(mensajeLayout);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(listaMensajes.get(position));
    }

    @Override
    public int getItemCount() {
        return listaMensajes.size();
    }

    public MensajeAdapter(List<Mensaje> mensajes){
        this.listaMensajes = mensajes;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void add(Mensaje mensaje){
        listaMensajes.add(mensaje);
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView mensaje;
        TextView horaMensaje;

        public MyViewHolder(@NonNull View view) {
            super(view);
            this.mensaje = view.findViewById(R.id.tvMensaje);
            this.horaMensaje = view.findViewById(R.id.tvHora);
        }

        public void bind(Mensaje mensaje){
            this.mensaje.setText(mensaje.getMensaje());
            this.horaMensaje.setText(mensaje.getTime());
        }
    }
}

