package pintegrador.equipo3.batoipop.Chat;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Timer;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class Mensaje implements Serializable {

    private String nombre;
    private String mensaje;
    private String time;
    private LocalDateTime locaDate;

    public Mensaje(String nombre, String mensaje){
        this.nombre = nombre;
        this.mensaje = mensaje;

        this.locaDate = LocalDateTime.now();
        this.time = locaDate.getHour() + ":" + locaDate.getMinute();
    }

    public Mensaje(){
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
