package pintegrador.equipo3.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SignInActivity extends AppCompatActivity {
    private boolean showPassword = false;
    private EditText etUsername, etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_page);

        setUI();
    }

    private void setUI() {
        etUsername = findViewById(R.id.etSignInUsername);
        etEmail = findViewById(R.id.etSignInEmail);
        etPassword = findViewById(R.id.pwSignIn);
    }

    public void showPassword(View view) {
        if (!showPassword) {
            showPassword = true;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            showPassword = false;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }

    }

    public void goBack(View view) {
        finish();
    }
}