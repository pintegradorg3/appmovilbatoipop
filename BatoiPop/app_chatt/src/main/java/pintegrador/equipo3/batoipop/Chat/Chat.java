package pintegrador.equipo3.batoipop.Chat;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.spec.InvalidKeySpecException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import pintegrador.equipo3.batoipop.R;

public class Chat extends AppCompatActivity implements View.OnClickListener{

    private final int PORT = 9098;
    private final String IP_SERVIDOR = "192.168.0.102";

    private RecyclerView recyclerView;
    private MensajeAdapter mensajeAdapter;
    private EditText etMensaje;
    private Button btSend;
    private ArrayList<Mensaje> chat;

    private final String CLAVE = "Alex Alonso Erian Victoria", SALT = "Batoipop", ALGORITHM = "AES/CBC/PKCS5Padding", IV = "hola caracola 12";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chat);

        if(onViewCreated(savedInstanceState))
        setUI();

    }

    private void setUI() {

        this.etMensaje = findViewById(R.id.tvMsn);
        this.btSend = findViewById(R.id.btSend);

        this.chat = new ArrayList<>();
        this.btSend.setOnClickListener(this);

        this.mensajeAdapter = new MensajeAdapter(chat);

        this.recyclerView = findViewById(R.id.reciclerViewChat);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        this.recyclerView.setAdapter(mensajeAdapter);

         onLine();
    }

    private void onLine() {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CHECK 1");
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            /**
             * Método creado para que el cliente esté a la escucha en _todo momento
             */
            @Override
            public void run() {
                try (ServerSocket serverSocket_cliente = new ServerSocket(PORT)){

                    Socket socketCliente;
                    SecretKey secretKey = AESCifrado.getKeyFromPassword(CLAVE, SALT);
                    IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));

                    while (true){
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CHECK 2");

                        socketCliente = serverSocket_cliente.accept();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketCliente.getInputStream()));
                        String nombreUsuario = bufferedReader.readLine();
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + nombreUsuario);
                        String mensajeCifrado = bufferedReader.readLine();
                        String mensajeDescifrado = AESCifrado.decrypt(ALGORITHM, mensajeCifrado, secretKey, iv);

                        mensajeAdapter.add(new Mensaje(nombreUsuario, mensajeDescifrado));
                    }
                } catch (IOException e) {
                    System.out.println("ERROR AL CREAR EL SOCKET DE SERVIDOR EN CLIENTE -> " + e.getMessage());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        String texto = "";
        try {
            SecretKey secretKey = AESCifrado.getKeyFromPassword(CLAVE, SALT);
            IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));

            Socket socket = new Socket(IP_SERVIDOR, PORT);
            texto = this.etMensaje.getText().toString();


            String finalTexto = texto;

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            bufferedWriter.write("1");
            bufferedWriter.newLine();
            bufferedWriter.write("Usser2: ");
            bufferedWriter.newLine();
            bufferedWriter.write(AESCifrado.encrypt(ALGORITHM, finalTexto, secretKey, iv));
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();

            Mensaje msn = new Mensaje("YO: ", texto);

//            // PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
//            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
//            bufferedWriter.write("Usser1: ");
//            bufferedWriter.newLine();
//            bufferedWriter.write(AESCifrado.encrypt(ALGORITHM, texto, secretKey, iv));
//            bufferedWriter.newLine();
//            bufferedWriter.flush();

            mensajeAdapter.add(msn);
            socket.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onViewCreated(Bundle savedInstanceState){
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            return true;
        } return false;
    }
}
