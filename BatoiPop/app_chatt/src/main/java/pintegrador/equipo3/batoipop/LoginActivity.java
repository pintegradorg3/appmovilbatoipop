package pintegrador.equipo3.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import pintegrador.equipo3.batoipop.Chat.Chat;

public class LoginActivity extends AppCompatActivity {

    private boolean showPassword = false;
    private EditText etUsername, etPassword;
    private Button btLogin;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);

        setUI();
    }

    private void setUI() {
        context = this;
        etUsername = findViewById(R.id.etLoginUsername);
        etPassword = findViewById(R.id.pwLogin);
        btLogin = findViewById(R.id.btLogIn);
    }

    public void showPassword(View view) {
        if (!showPassword) {
            showPassword = true;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            showPassword = false;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }

    }

    public void register(View view) {
        Intent i = new Intent(this, Chat.class);
        startActivity(i);
    }
}