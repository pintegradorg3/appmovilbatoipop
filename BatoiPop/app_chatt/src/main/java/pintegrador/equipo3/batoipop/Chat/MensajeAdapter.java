package pintegrador.equipo3.batoipop.Chat;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import pintegrador.equipo3.batoipop.R;

public class MensajeAdapter extends RecyclerView.Adapter<MensajeAdapter.MyViewHolder>{

    ArrayList<Mensaje> listaMensajes;

    @NonNull
    @Override
    public MensajeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mensajeLayaut = LayoutInflater.from(parent.getContext()).inflate(R.layout.mensaje, parent, false);
        return new MyViewHolder(mensajeLayaut);
    }

    @Override
    public void onBindViewHolder(@NonNull MensajeAdapter.MyViewHolder holder, int position) {
        holder.bind(listaMensajes.get(position));
    }

    @Override
    public int getItemCount() {
        return listaMensajes.size();
    }

    public MensajeAdapter(ArrayList<Mensaje> chat){
        this.listaMensajes = chat;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void add(Mensaje mensaje){
        listaMensajes.add(mensaje);
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nombreUsuario;
        TextView mensaje;
        TextView horaMensaje;
        View view;

        public MyViewHolder(@NonNull View view) {
            super(view);
            this.view = view;
            this.nombreUsuario = view.findViewById(R.id.tvNombreUsuario);
            this.mensaje = view.findViewById(R.id.tvMensaje);
            this.horaMensaje = view.findViewById(R.id.tvHoraMensaje);
        }

        public void bind(Mensaje mensaje){
            this.nombreUsuario.setText(mensaje.getNombre());
            this.mensaje.setText(mensaje.getMensaje());
            this.horaMensaje.setText(mensaje.getTime());
        }
    }
}

